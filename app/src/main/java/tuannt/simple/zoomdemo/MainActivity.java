package tuannt.simple.zoomdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import us.zoom.sdk.JoinMeetingOptions;
import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKInitParams;
import us.zoom.sdk.ZoomSDKInitializeListener;

public class MainActivity extends AppCompatActivity implements InputDialog.NoticeDialogListener {

    private static final String LOG_TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static String fileName = null;

    private RecordButton recordButton = null;
    private MediaRecorder recorder = null;

    private PlayButton   playButton = null;
    private MediaPlayer player = null;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    // save text
    private String storeText = "";

    private SpeechRecognizer mSpeechRecognizer;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String id, String password) {
        if (id == null || password == null) {
            return;
        }
        if (id.trim().isEmpty() || password.trim().isEmpty()) {
            return;
        }
        joinMeeting(getBaseContext(), id, password);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    class RecordButton extends Button {
        boolean mStartRecording = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onRecord(mStartRecording);
                if (mStartRecording) {
                    setText("Stop recording");
                } else {
                    setText("Start recording");
                }
                mStartRecording = !mStartRecording;
            }
        };

        public RecordButton(Context ctx) {
            super(ctx);
            setText("Start recording");
            setOnClickListener(clicker);
        }
    }

    class PlayButton extends Button {
        boolean mStartPlaying = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onPlay(mStartPlaying);
                if (mStartPlaying) {
                    setText("Stop playing");
                } else {
                    setText("Start playing");
                }
                mStartPlaying = !mStartPlaying;
            }
        };

        public PlayButton(Context ctx) {
            super(ctx);
            setText("Start playing");
            setOnClickListener(clicker);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Record to the external cache directory for visibility
        fileName = getFilesDir().getAbsolutePath();
        fileName += "/audiorecordtest.3gp";

        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }

        Log.i(">>>>", fileName);

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        LinearLayout ll = new LinearLayout(this);
        Button joinButton = new Button(this);
        joinButton.setText("Join Meeting");
        joinButton.setOnClickListener(view -> {
            DialogFragment dialog = new InputDialog();
            dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
//            joinMeeting(getBaseContext(), "88205808926", "mjYc3K");
        });

        ll.addView(joinButton, new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0));

        recordButton = new RecordButton(this);
        ll.addView(recordButton,
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        0));
        playButton = new PlayButton(this);
        ll.addView(playButton,
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        0));
        setContentView(ll);

        initializeSdk(this);


        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);


        final Intent mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());


        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                //getting all the matches
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                //displaying the first match
                if (matches != null)
                    storeText += matches.get(0) + "\n";
                    Log.i(">>>>", storeText);
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });

//        setContentView(R.layout.activity_main);
//        initializeSdk(this);
//
//        Button joinButton = findViewById(R.id.join_button);
//        joinButton.setOnClickListener(view -> {
//            joinMeeting(getBaseContext(), "88205808926", "mjYc3K");
//        });
//
//
//        Button speakButton = findViewById(R.id.start_listen);
//        speakButton.setOnClickListener(view -> {
////            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
////            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
////                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
////            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
////            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
////                    "Test");
////            try {
////                startActivityForResult(intent, 100);
////            } catch (ActivityNotFoundException a) {
////                Toast.makeText(getApplicationContext(),
////                        "speech not support",
////                        Toast.LENGTH_SHORT).show();
////            }
//
//
//        });
//
//        textResult = findViewById(R.id.text_title);
    }

    private void onRecord(boolean start) {
        if (start) {
//            final Intent mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//            mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//            mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
//                    Locale.getDefault());
//            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
            startRecording();
        } else {
//            mSpeechRecognizer.stopListening();
            stopRecording();
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        player.release();
        player = null;
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        recorder = null;
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // Must be called after setAudioSource(int) and before setOutputFormat(int).
            recorder.setPrivacySensitive(true);
        }
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e("test", "prepare() failed");
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//            recorder.setPrivacySensitive(true);
//        }
        recorder.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }

        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//                    textResult.setText(result.get(0));
                }
                break;
            }

        }
    }


    public void initializeSdk(Context context) {
        ZoomSDK sdk = ZoomSDK.getInstance();
        // TODO: For the purpose of this demo app, we are storing the credentials in the client app itself. However, you should not use hard-coded values for your key/secret in your app in production.
        ZoomSDKInitParams params = new ZoomSDKInitParams();
        params.appKey = "bBTwX3Ss2C8CMGv3XYU6u1Ubht8XDyZbrmHB"; // TODO: Retrieve your SDK key and enter it here
        params.appSecret = "uGGkk3llNTQTfDHuSKtAp4auhc5euTEQKDyv"; // TODO: Retrieve your SDK secret and enter it here
        params.domain = "zoom.us";
        params.enableLog = true;
        // TODO: Add functionality to this listener (e.g. logs for debugging)
        ZoomSDKInitializeListener listener = new ZoomSDKInitializeListener() {
            /**
             * @param errorCode {@link us.zoom.sdk.ZoomError#ZOOM_ERROR_SUCCESS} if the SDK has been initialized successfully.
             */
            @Override
            public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
                Log.i(">>>>", "" + errorCode);
            }
            @Override
            public void onZoomAuthIdentityExpired() {

            }
        };
        sdk.initialize(context, listener, params);
    }



    // Write the joinMeeting function.
    private void joinMeeting(Context context, String meetingNumber, String password) {
        MeetingService meetingService = ZoomSDK.getInstance().getMeetingService();
        JoinMeetingOptions options = new JoinMeetingOptions();
        JoinMeetingParams params = new JoinMeetingParams();
        params.displayName = "tuanbaros"; // TODO: Enter your name
        params.meetingNo = meetingNumber;
        params.password = password;
        meetingService.joinMeetingWithParams(context, params, options);
    }

//    private fun createJoinMeetingDialog() {
//        new AlertDialog.Builder(this)
//                .setView(R.layout.dialog_main)
//                .setPositiveButton("Join") { dialog, _ ->
//                dialog as AlertDialog
//            val numberInput = dialog.findViewById<TextInputEditText>(R.id.meeting_no_input)
//                    val passwordInput = dialog.findViewById<TextInputEditText>(R.id.password_input)
//                    val meetingNumber = numberInput?.text?.toString()
//            val password = passwordInput?.text?.toString()
//            meetingNumber?.takeIf { it.isNotEmpty() }?.let { meetingNo ->
//                    password?.let { pw ->
//                    joinMeeting(this@MainActivity, meetingNo, pw)
//            }
//            }
//            dialog.dismiss()
//        }
//        .show()
//    }
}